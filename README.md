# Sebin Dynamic Wallpaper

Dynamic wallpaper for the GNOME and KDE Plasma desktop environment, featuring Sebin working from home by [Tyr](#credits), as installable Arch Linux package.

Wallpaper changes at the following times of day:

* 07:00 - 10:00: Sebin makes coffee and breakfast
* 10:00 - 13:00: Sebin starts working
* 13:00 - 14:00: Sebin has lunch
* 14:00 - 17:00: Sebin works out
* 17:00 - 20:00: Sebin plays video games
* 20:00 - 23:00: Sebin talks to friends online
* 23:00 - 07:00: Sebin sleeps on the couch

To determine the solar altitude/azimuth values [this Keisan Online Calculator](https://keisan.casio.com/exec/system/1224682277) is a good place to start.

## Install

### Arch Linux (and derivatives)

To build the dynamic wallpaper packages simply clone this repository and run:

~~~bash
makepkg -s
~~~

Then install the package for your desktop:

~~~bash
# GNOME
pacman -U sebin-dynamic-wallpaper-1*.pkg.tar.zst

# KDE Plasma 
# ⚠️ NOTE: the KDE Plasma version has a dependency to an AUR package: plasma5-wallpapers-dynamic
# Consider using an AUR helper, e.g. yay
pacman -U sebin-dynamic-wallpaper-kde-*.pkg.tar.zst
~~~

The wallpaper should show up in your desktop's wallpaper settings.

For KDE you need to switch the wallpaper type to "Dynamic" to use it with the dynamic wallpaper plugin.

Select it and enjoy a different wallpaper at different times throughout the day ✨

### Manual (every other distro)

#### GNOME

Clone this repository and copy the files to either:

* `~/.local/share/backgrounds/` (single user)
* `/usr/share/backgrounds/sebin-dynamic-wallpaper/` (system-wide)

~~~bash
# single user
cp sebin-*.(png|xml) ~/.local/share/backgrounds/sebin-dynamic-wallpaper/
cp sebin.xml ~/.local/share/gnome-background-properties/

# system-wide
sudo cp sebin-*.(png|xml) /usr/share/backgrounds/sebin-dynamic-wallpaper/
sudo cp sebin.xml /usr/share/gnome-background-properties/
~~~

#### KDE Plasma

For KDE the [Plasma Dynamic Wallpaper Engine](https://github.com/zzag/plasma5-wallpapers-dynamic) is required for this wallpaper to work.

Clone this repository and build the AVIF file:

~~~bash
kdynamicwallpaperbuilder "sebin-dynamic-kde.json" --output "3840x2160.avif" --speed 10
~~~

Then copy files to either of these locations:

* `~/.local/share/wallpapers/` (single user)
* `/usr/share/wallpapers/sebin-dynamic-wallpaper-kde/` (system-wide)

~~~bash
# single user
mkdir -p ~/.local/share/wallpapers/sebin-dynamic-wallpaper-kde/contents/images

cp metadata.desktop ~/.local/share/wallpapers/sebin-dynamic-wallpaper-kde/metadata.desktop
cp screenshot.jpg ~/.local/share/wallpapers/sebin-dynamic-wallpaper-kde/contents/screenshot.jpg
cp 3840x2160.avif ~/.local/share/wallpapers/sebin-dynamic-wallpaper-kde/contents/images/3840x2160.avif

# system-wide
sudo mkdir -p /usr/share/wallpapers/sebin-dynamic-wallpaper-kde/contents/images

sudo cp metadata.desktop /usr/share/wallpapers/sebin-dynamic-wallpaper-kde/metadata.desktop
sudo cp screenshot.jpg /usr/share/wallpapers/sebin-dynamic-wallpaper-kde/contents/screenshot.jpg
sudo cp 3840x2160.avif /usr/share/wallpapers/sebin-dynamic-wallpaper-kde/contents/images/3840x2160.avif
~~~

### macOS

On macOS use [wallpapper](https://github.com/mczachurski/wallpapper) to create an HEIC image that you can set as dynamic wallpaper in preferences:

~~~bash
# based on time of day
wallpapper -i sebin-timed.json -o sebin-dyn_wallpaper.heic

# based on solar altitude/azimuth
wallpapper -i sebin-sun.json -o sebin-dyn_wallpaper.heic
~~~

### Windows

See [this How-to-geek article](https://www.howtogeek.com/355912/how-to-change-windows-10s-wallpaper-based-on-time-of-day/).

## Credits

Wallpaper made by Tyr

* Instagram: https://www.instagram.com/TyrKilcat/
* Twitter: https://twitter.com/TyrKilcat
* Fur Affinity: https://www.furaffinity.net/user/tyrart/
* Telegram: https://t.me/Tyrkilkat
